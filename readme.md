# Revolt Example Bot (helper-bot)

This is a minimal example of coding a Bot for the [Revolt](https://app.revolt.chat) chat system using `Rust`.

More information on the admin work required in Revolt and testing your Bot can be found in my FAQ,
[The Revolt Bot FAQ](https://jesusmachinesantana.bitbucket.io/blog/2021-10-16-the-revolt-bot-faq.html).

## building and running

First you will need your

 * Bot (SecretToken and UserId)
 * your Revolt UserId
 * your new Channel Id for Bot testing
 
 (see FAQ above for getting these)
 
After replacing the placeholders for these in `src/main.rs` you can build and run the bot.

```shell
    $ cargo run
```

The bot should now connect to the Revolt channel you created for it (see the FAQ above).
Typing the command `/help` in the same channel should get a response from `helper-bot`.

That's all it does, but it should be enough to get you started writing your own Bot.

You should see lots of logging in the `cargo run` terminal that will let you know your bot is working
(or has failed to connect).

The minimum json api I needed for this Bot is described in `src/protocol.rs` but there is much more to the
[Revolt api](https://developers.revolt.chat).
