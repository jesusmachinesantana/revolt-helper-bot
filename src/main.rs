use futures_util::{
    stream::{SplitSink, SplitStream},
    SinkExt, StreamExt,
};
use helper_bot::protocol;
use std::io;
use tokio_tungstenite::tungstenite;
use tokio_tungstenite::{MaybeTlsStream, WebSocketStream};
use tungstenite::Message;

const WEBSOCKET_URL: &str = "wss://ws.revolt.chat";
const HTTP_URL: &str = "https://api.revolt.chat";
const BOT_TOKEN: &str = "MyBotToken";
const BOT_USER_ID: &str = "MyBotUserId";
const CHANNEL_ID: &str = "MyBotChannelId";
const PING_INTERVAL_SECS: u64 = 15;

type WsReader = SplitStream<WebSocketStream<MaybeTlsStream<tokio::net::TcpStream>>>;
type WsWriter = SplitSink<WebSocketStream<MaybeTlsStream<tokio::net::TcpStream>>, Message>;
type HyperHttpsClient =
    hyper::Client<hyper_tls::HttpsConnector<hyper::client::HttpConnector>, hyper::Body>;

struct Context {
    ws_reader: WsReader,
    ws_writer: WsWriter,
    http_client: HyperHttpsClient,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (mut ws_writer, mut ws_reader) = websocket_connect(WEBSOCKET_URL).await;

    let http_client = https_create().await;

    send_authentication(&mut ws_writer, BOT_TOKEN).await?;

    authorizing_state_loop(&mut ws_reader).await?;

    let context = Context {
        ws_reader,
        ws_writer,
        http_client,
    };

    let timer_rx = interval_ping_generator(PING_INTERVAL_SECS).await;

    active_state_loop(context, timer_rx).await
}

async fn websocket_connect(url: &str) -> (WsWriter, WsReader) {
    let (ws_stream, _) = tokio_tungstenite::connect_async(url)
        .await
        .expect("failed to connect to websocket");
    ws_stream.split()
}
async fn https_create() -> HyperHttpsClient {
    let https = hyper_tls::HttpsConnector::new();
    hyper::Client::builder().build::<_, hyper::Body>(https)
}
async fn send_authentication(
    ws_writer: &mut WsWriter,
    bot_token: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let authmsg = tungstenite::Message::Text(serde_json::to_string(&protocol::Authenticate::new(
        bot_token,
    ))?);
    ws_writer.send(authmsg).await.or_else(|_e| {
        Err(Box::new(HelperError::new("write auth message failed")) as Box<dyn std::error::Error>)
    })
}

async fn authorizing_state_loop(
    ws_reader: &mut WsReader,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let msg = ws_reader.next().await.expect("cannot get incoming message");
        match msg?.into_text() {
            Ok(text) => {
                if authorizing_state(&text)? {
                    break;
                }
            }
            Err(e) => {
                eprintln!("cannot convert incoming message to text: {}", e);
            }
        }
    }
    Ok(())
}

async fn active_state_loop(
    mut ioctx: Context,
    mut timer_rx: tokio::sync::mpsc::Receiver<bool>,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        tokio::select! {
          msg = ioctx.ws_reader.next() => {
            match msg {
              Some(text) => {
                let text = text?.into_text().expect("cannot convert ws message to text");
                if active_state(&text, &mut ioctx).await? {
                  panic!("exiting the server cleanly...jk");
                }
              }
              None=> {
                println!("socket channel was closed");
              }
            }
          }
          _tmr = timer_rx.recv() => {
                let pingmsg = tungstenite::Message::Text(
                    serde_json::to_string(&protocol::Ping::new()).unwrap(),
                );
                println!("sending ping {:?}", pingmsg);
                ioctx.ws_writer.send(pingmsg).await.expect("failed to send ping");
          }
        }
    }
}

fn authorizing_state(json_text: &str) -> io::Result<bool> {
    println!("authorizing_state message: {}", json_text);
    match serde_json::from_str::<protocol::Authorizing>(json_text)? {
        protocol::Authorizing::Authenticated {} => {
            println!("authenticated message received");
            return Ok(false);
        }
        protocol::Authorizing::Ready { users, .. } => {
            println!("ready message users {:#?}", users);
            return Ok(true);
        }
    }
}

async fn active_state(json_text: &str, ioctx: &mut Context) -> io::Result<bool> {
    println!("active_state message: {}", json_text);
    match serde_json::from_str::<protocol::Active>(json_text)? {
        protocol::Active::Message {
            channel,
            author,
            content,
            ..
        } => {
            handle_command(ioctx, &channel, &author, &content).await;
            return Ok(false);
        }
        protocol::Active::Pong { data } => {
            println!("pong message unix epoch secs {}", data);
            return Ok(false);
        }
        protocol::Active::UserUpdate {} => {
            println!("user update message received, ignoring: {}", json_text);
            return Ok(false);
        }
        protocol::Active::ChannelStartTyping { user, .. } => {
            println!("user {} started typing.", user);
            return Ok(false);
        }
        protocol::Active::ChannelStopTyping { user, .. } => {
            println!("user {} stopped typing.", user);
            return Ok(false);
        }
    }
}

static BEGIN_TYPING: &str =
    "{\"type\":\"BeginTyping\", \"channel\":\"01FJ23WZQEK2GKEJPK1CXV0XKV\"}";
static END_TYPING: &str = "{\"type\":\"EndTyping\", \"channel\":\"01FJ23WZQEK2GKEJPK1CXV0XKV\"}";

async fn handle_command(ioctx: &mut Context, channel: &str, author: &str, content: &str) {
    if author == BOT_USER_ID || channel != CHANNEL_ID {
        return;
    }
    match content {
        "/help" => {
            send_text_message(
                ioctx,
                [
                    "# helper bot",
                    "a minimal demonstration bot for the **Revolt** chat system.",
                ]
                .join("\n")
                .as_str(),
            )
            .await;
        }
        _ => {}
    }
}
async fn send_text_message(ioctx: &mut Context, text: &str) {
    let message = serde_json::to_string(&protocol::SentMessage::new(String::from(text))).unwrap();
    println!("sending {}", message);
    ioctx
        .ws_writer
        .send(tungstenite::Message::Text(BEGIN_TYPING.to_string()))
        .await
        .expect("failed to send begin_typing");
    let http_request = hyper::Request::builder()
        .method(hyper::Method::POST)
        .uri(&format!("{}/channels/{}/messages", HTTP_URL, CHANNEL_ID))
        .header(hyper::header::CONTENT_TYPE, "application/json")
        .header(hyper::header::USER_AGENT, "helper-bot v0.1")
        .header("x-bot-token", BOT_TOKEN)
        .body(hyper::Body::from(message))
        .expect("failed to build http message");
    let resp = ioctx.http_client.request(http_request).await;
    println!("send message: http response: {:?}", resp);
    ioctx
        .ws_writer
        .send(tungstenite::Message::Text(END_TYPING.to_string()))
        .await
        .expect("failed to send end_typing");
}
async fn interval_ping_generator(interval_secs: u64) -> tokio::sync::mpsc::Receiver<bool> {
    let (timer_tx, timer_rx) = tokio::sync::mpsc::channel(1);
    tokio::spawn(async move {
        let mut interval = tokio::time::interval(std::time::Duration::from_secs(interval_secs));
        loop {
            interval.tick().await;
            timer_tx
                .send(true)
                .await
                .expect("timer channel send failed");
        }
    });
    timer_rx
}
#[derive(Debug)]
struct HelperError {
    msg: String,
}
impl HelperError {
    fn new(msg: &str) -> HelperError {
        HelperError {
            msg: String::from(msg),
        }
    }
}
impl std::error::Error for HelperError {
    fn description(&self) -> &str {
        &self.msg
    }
}
impl std::fmt::Display for HelperError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "HelperError({})", self.msg)
    }
}
