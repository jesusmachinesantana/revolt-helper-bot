use serde::{self, Deserialize, Serialize};
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug, Serialize, Deserialize)]
pub struct Authenticate {
    #[serde(rename = "type")]
    _type: String,
    token: String,
}
impl Authenticate {
    pub fn new(token: &str) -> Authenticate {
        Authenticate {
            _type: "Authenticate".to_string(),
            token: token.to_string(),
        }
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Ping {
    #[serde(rename = "type")]
    _type: String,
    data: u64,
}
impl Ping {
    pub fn new() -> Ping {
        Ping {
            _type: "Ping".to_string(),
            data: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        }
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct SentMessage {
    #[serde(rename = "type")]
    pub _type: String,
    pub nonce: String,
    pub content: String,
}

impl SentMessage {
    pub fn new(content: String) -> SentMessage {
        SentMessage {
            _type: String::from("Message"),
            nonce: ulid::Ulid::new().to_string(),
            content,
        }
    }
}

// incoming messages

// 2021-10-21 12:43:30: change struct types to enum/tag suggested by revolt::@OneAutumnLeaf author of robespierre

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Authorizing {
    Authenticated {},
    Ready {
        users: Vec<User>,
        servers: Vec<Server>,
        channels: Vec<Channel>,
    },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Active {
    Pong {
        data: usize,
    },
    Message {
        _id: String,
        nonce: String,
        channel: String,
        author: String,
        content: String,
    },
    UserUpdate {},
    ChannelStartTyping {
        id: String,
        user: String,
    },
    ChannelStopTyping {
        id: String,
        user: String,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub _id: String,
    pub username: String,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Server {
    pub _id: String,
    pub channels: Vec<String>,
    pub default_permissions: (usize, usize),
    pub name: String,
    pub owner: String,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Channel {
    pub _id: String,
    pub name: String,
    pub channel_type: String,
    pub server: String,
    pub nonce: String,
    pub description: String,
    pub last_message_id: String,
}
